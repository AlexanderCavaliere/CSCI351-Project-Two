/**
 * client.c
 * Alexander R. Cavaliere <arc6393@rit.edu>
 */

#include "project_headers.h"



in_flight_segment * build_congestion_window( int input_file, uint32_t congestion_window, uint32_t mss,
                                             in_flight_segment * first_segment, uint32_t start_sequence_number )
{
  int blocks_to_read = congestion_window / mss;
  int remaining_amount_to_read = blocks_to_read % mss;
  uint8_t * chunk = malloc( (mss - sizeof( fake_tcp_header ) ) * sizeof( uint8_t ) );
  in_flight_segment * first_segment_storage = first_segment;

  for( int reads = 0; reads < blocks_to_read; ++reads )
    {
      int read_bytes = read( input_file, chunk, mss - sizeof( fake_tcp_header ) );
      if( first_segment == NULL ) // We haven't sent any data out
        {
          first_segment->sequence_number = start_sequence_number;
          //first_segment->data = chunk;
          memcpy( first_segment->data, chunk, mss - sizeof( fake_tcp_header ) );
          first_segment->ACK = 0;
          first_segment->next_segment = NULL;
        }
      else if( reads == 0 ) // We just entered the function
        {
          first_segment->sequence_number = start_sequence_number;
          //first_segment->data = chunk;
          memcpy( first_segment->data, chunk, mss - sizeof( fake_tcp_header ) );
          first_segment->ACK = 0;
          first_segment->next_segment = NULL;
        }
      else // We are continuing to create the congestion window
        {
          in_flight_segment * inter_segment;
          inter_segment = malloc( sizeof( in_flight_segment ) );
          inter_segment->sequence_number = start_sequence_number;
          //inter_segment->data = chunk;
          memcpy( inter_segment->data, chunk, mss - sizeof( fake_tcp_header ) );
          inter_segment->ACK = 0;
          inter_segment->next_segment = NULL;

          first_segment->next_segment = inter_segment;
          first_segment = first_segment->next_segment;
        }
      start_sequence_number += (mss - sizeof( fake_tcp_header ) );
    }
  free( chunk ); // Clean up
  if( remaining_amount_to_read > 0 )
    {
      chunk = malloc( remaining_amount_to_read * sizeof( uint8_t ) );
      int read_bytes = read( input_file, chunk, remaining_amount_to_read * sizeof( uint8_t ) );
      in_flight_segment * last_segment;
      last_segment = malloc( sizeof( in_flight_segment ) );
      last_segment->sequence_number = start_sequence_number;
      last_segment->ACK = 0;
      last_segment->next_segment = NULL;
    }
}// End build_congestion_window


void acknowledge_in_flight_segments( in_flight_segment * first_segment, uint32_t acknowledgement_number )
{
  in_flight_segment * first_temp = first_segment;
  while( first_temp->next_segment != NULL )
    {
      if( first_temp->sequence_number <= acknowledgement_number )
        {
          first_temp->ACK = 1;
          first_temp = first_temp->next_segment;
          continue;
        }
      break;
    }
  if( first_temp->sequence_number <= acknowledgement_number )
    first_temp->ACK = 1;
}// End acknowledge_in_flight_segments


void clean_up_acked_segments( in_flight_segment * first_segment )
{
  in_flight_segment * first_temp = first_segment;

  while( first_temp->next_segment != NULL )
    {
      if( first_temp->ACK == 1 )
        {
          free( first_temp->data );
          in_flight_segment * temp = first_temp;
          first_temp = first_temp->next_segment;
          free( temp );
          continue;
        }
      break;
    }
}// End clean_up_acked_segments


void perform_transfer( fake_tcp_header * header, int socket, struct sockaddr_in server, socklen_t address_size,
                       fake_tcp_header incoming, struct timeval timeout, fd_set* fd_sets,
                       int input_file )
{
  print_header_values( *header );
  // Hash Variables
  SHA_CTX ctx;
  SHA1_Init( &ctx );


  set_header_acknowledge_field( header, 0 );
  set_header_sync_field( header, 0 );

  // Keep a backup around in case the struct is modified by the system
  int timeout_backup        = timeout.tv_usec;


  // Congestion Control States
  bool SLOW_START = true;
  bool CONGESTION_AVOIDANCE = false;
  bool FAST_RECOVERY = false;


  // Transmission Defaults
  int DATA_SIZE             = 1024;
  int MSS                   = 1040;

  uint8_t * chunk           = malloc( DATA_SIZE * sizeof( uint8_t ) );
  void * message_payload    = malloc( MSS * sizeof( uint8_t ) );

  int message_received, message_outgoing, reader;

  uint32_t next_sequence_number  = header->sequence_number;
  uint32_t send_base             = header->sequence_number;
  uint32_t congestion_window     = MSS;
  int ss_threshold          = 64000;

  bool read_data            = true;

  int duplicate_acknowledgements = 0;

  uint32_t message_received_count = 0;
  uint32_t message_outgoing_count = 0;
  // Data Transmission Loop
  do
    {
      if( read_data )
        {
          /*
            Make a "segment" struct where we keep track of:
            1) Sequence Number
            2) ACK status
            3) Segment data
            These can be used to keep track of inflight segments
            Store them in a dynamic array (Or just put a "next" pointer on it...)
           */
          reader = read( input_file, chunk, DATA_SIZE );

          next_sequence_number += header->sequence_number;

          // Checksum Calculation
          set_header_checksum_field( header, 0 );
          header->checksum = calculate_checksum( *header, chunk, DATA_SIZE );

          printf( "OUTGOING HEADER VALUES:\n" );
          print_header_values( *header );
          memcpy( message_payload, header, sizeof( *header ) );

          /* fake_tcp_header test_header; */
          /* memcpy( &test_header, message_payload, sizeof( test_header ) ); */
          /* printf( "TEST HEADER VALUES:\n" ); */
          /* print_header_values( test_header ); */


          message_payload += sizeof( *header );
          memcpy( message_payload, (uint8_t*)chunk, DATA_SIZE );

          SHA1_Update( &ctx, chunk, DATA_SIZE );
          message_payload -= sizeof( *header );

        }

      read_data = false;

      message_outgoing = sendto( socket, message_payload, MSS, 0, (struct sockaddr *)&server, sizeof(server) );

      if( message_outgoing < 0 )
        perror( "sendto" );


      // Select() section
      FD_ZERO( &(fd_sets[SELECT_READ_SETS]) );
      FD_SET( socket, &(fd_sets[SELECT_READ_SETS]) );

      int select_message = select( socket + 1, &(fd_sets[SELECT_READ_SETS]), NULL, NULL, &timeout );


      if( FD_ISSET( socket, &(fd_sets[SELECT_READ_SETS]) ) )
        {
          // We have received a message from the server
          printf( "We have received a message from the server!\n" );

          message_received = recvfrom( socket, &incoming, sizeof( incoming ), 0,
                                       (struct sockaddr *)&server, &address_size );

          printf( "MESSAGE RECEIVED HEADER VALUES:\n" );
          print_header_values( incoming );

          // Make sure the message came back intact
          if( verify_checksum( incoming.checksum, incoming, (uint8_t *)&incoming, 0 ) )
            {
              if( incoming.acknowledge == 1 )
                {
                  printf( "We have received an ACK!\n" );
                  if( incoming.acknowledgement_number > send_base && incoming.acknowledgement_number != send_base )
                    {
                      printf( "We have ACK'd the data!\n" );
                      send_base = incoming.acknowledgement_number;
                      read_data = true;

                      // Handle setting up the response header
                      set_header_sequence_number( header, incoming.acknowledgement_number );
                      set_header_acknowledgement_number( header, incoming.sequence_number + 1 );

                      if( SLOW_START )
                        congestion_window += MSS;

                      if( CONGESTION_AVOIDANCE )
                        congestion_window += ( MSS * ( MSS / congestion_window ) );

                      duplicate_acknowledgements = 0; // We've received a new ACK

                      if( congestion_window > ss_threshold && !CONGESTION_AVOIDANCE )
                        {
                          // We've moved to CONGESTION AVOIDANCE from SLOW START
                          CONGESTION_AVOIDANCE  = true;
                          SLOW_START            = false;
                          FAST_RECOVERY         = false;
                        }

                    }
                  else
                    {
                      duplicate_acknowledgements++; // We've received the same ACK (Something's gone wrong!)
                      if( duplicate_acknowledgements == 3 )
                        {
                          if( !FAST_RECOVERY )
                            {
                              // We've moved to FAST RECOVERY
                              SLOW_START            = false;
                              CONGESTION_AVOIDANCE  = false;
                              FAST_RECOVERY         = true;

                              ss_threshold = congestion_window / 2;
                              congestion_window = ss_threshold + 3;
                              // Retransmit the missing segment
                              read_data = false;
                            }
                          else
                            {
                              congestion_window += MSS;
                              // Transmit new segment(s) as allowed
                            }
                        }
                    }
                }// End receive ACK section
            }
        }
      else
        {
          // Timeout occurred
          if( SLOW_START )
            {
              ss_threshold = congestion_window / 2;
              congestion_window = MSS;
              duplicate_acknowledgements = 0;
              // Retransmit the un-ACK'd segment
            }
          if( CONGESTION_AVOIDANCE )
            {
              // We've moved to SLOW START from CONGESTION AVOIDANCE
              SLOW_START            = true;
              CONGESTION_AVOIDANCE  = false;
              FAST_RECOVERY         = false;

              ss_threshold = congestion_window / 2;
              congestion_window = MSS;
              duplicate_acknowledgements = 0;
              // Retransmit un-ACK'd segment
            }
          if( FAST_RECOVERY )
            {
              // We've moved to SLOW START from FAST RECOVERY
              SLOW_START            = true;
              CONGESTION_AVOIDANCE  = false;
              FAST_RECOVERY         = false;

              ss_threshold = congestion_window / 2;
              congestion_window = MSS;
              duplicate_acknowledgements = 0;
              // Retransmit the un-ACK'd segment
            }
        }

      timeout.tv_usec = timeout_backup;
      //printf( "%d\n", message );
      //printf( "Message Sent\n" );
      //printf( "Reader = %d\n", reader );
    }
  while( reader > 0 );

  unsigned char hash[SHA_DIGEST_LENGTH];
  SHA1_Final( hash, &ctx );
  print_bytes( hash, SHA_DIGEST_LENGTH );
}// End perform_transfer


void perform_connection_setup( fake_tcp_header * header, int socket, struct sockaddr_in server, struct sockaddr_in client,
                               socklen_t address_size, fake_tcp_header incoming, struct timeval timeout, fd_set* fd_sets )
{
  // Keep a backup around in case the struct is modified by the system
  int timeout_backup = timeout.tv_usec;


  // Initialize the values in the header (zero out what will not be used)
  zero_out_header_values( header );
  set_header_sequence_number( header, 1 );
  set_header_sync_field( header, 1 );

  header->checksum = calculate_checksum( *header, (uint8_t*)header, 0 );

  // Zero out the incoming header
  zero_out_header_values( &incoming );

  //print_header_values( *header );
  printf( "Socket FD = %d\n", socket );
  // End header initialization


  // Send out the initialize sync request
  int message_out = sendto( socket, header, sizeof( *header ), 0,
                            (struct sockaddr *)&server, sizeof( server ) );

  printf( "Initial Handshake Payload Size = %d\n", message_out );

  if( message_out < 0 )
    {
      perror("sendto");
    }

  // Handshaking Section
  bool handshake_complete = false;
  int message_incoming;
  while( !handshake_complete ) // Begin the handshaking!
    {
      // Reset the read set for select and re-add the socket to the set
      FD_ZERO( &(fd_sets[SELECT_READ_SETS]) );
      FD_SET( socket, &(fd_sets[SELECT_READ_SETS]) );

      // Wait for data to appear
      int select_message = select( socket + 1, &(fd_sets[SELECT_READ_SETS]), NULL, NULL, &timeout );

      if( select_message < 0 )
        perror( "select ");

      bool resend_message = true;

      if( FD_ISSET( socket, &(fd_sets[SELECT_READ_SETS]) ) >= 1 )
        { // We have received data from the server
          printf( "The server has gotten back to us!\n" );
          message_incoming = recvfrom( socket, &incoming, sizeof( incoming ), 0,
                                       (struct sockaddr *)&server, &address_size );


          printf( "INCOMING HEADER VALUES:\n" );
          print_header_values( incoming );


          if( verify_checksum( incoming.checksum, incoming, (uint8_t *)&incoming, 0) )
            { // Verify the data's integrity
              if( ( incoming.acknowledgement_number == header->sequence_number + 1 ) &&
                  ( incoming.sync == 1 ) && ( incoming.acknowledge == 1 ) )
                { // This case is for after the server has sent us their sin-ack
                  // Send ack response to syn-ack
                  resend_message = false;

                  // Prepare outgoing header
                  zero_out_header_values( header );
                  set_header_sequence_number( header, incoming.acknowledgement_number );
                  set_header_acknowledgement_number( header, incoming.sequence_number );
                  set_header_acknowledge_field( header, 1 );

                  header->checksum = calculate_checksum( *header, (uint8_t *)header, 0 );

                  message_out = sendto( socket, header, sizeof( *header ), 0,
                                        (struct sockaddr *)&server, address_size );


                  printf( "OUTGOING HEADER VALUES:\n" );
                  print_header_values( *header );
                  if( message_out < 0 )
                    perror( "sendto" );


                  handshake_complete = true;
                }// We've informed the server of our intent and are now able to send data
            }
        }
      if( resend_message && FD_ISSET( socket, &(fd_sets[SELECT_WRITE_SETS]) ) >= 1 )
        // Should the server have not given a response or something was wrong with the message
        {
          message_out = sendto( socket, header, sizeof( *header ), 0,
                                (struct sockaddr *)&server, sizeof( server ));
          if( message_out < 0)
            printf( "Message Sent Out Error = %d\n", errno );

        }

      timeout.tv_usec = timeout_backup; // Unsure if system is modifying struct; play it safe
    } // End the handshaking!

}// End perform_connection_setup

void client( char* file_name, int timeout, int verbose, char* server_address, int port )
{
  // Socket Vars
  struct hostent* he;
  struct sockaddr_in server;
  struct sockaddr_in client_rec;
  struct sockaddr_storage incoming;
  socklen_t address_size;


  // Timeout Struct
  struct timeval select_timeout;
  select_timeout.tv_usec = timeout * 1000;
  select_timeout.tv_sec = 0;


  // Socket Creation
  int client_sck = socket( AF_INET, SOCK_DGRAM, 0 );

  client_rec.sin_family = AF_INET;
  client_rec.sin_port = htons( port - 1 );
  client_rec.sin_addr.s_addr = htonl( INADDR_ANY );
  // End Socket Creation


  // select() FD set initialization
  fd_set read_fd_set;
  fd_set write_fd_set;

  FD_ZERO( &read_fd_set );  // Zero out the set
  FD_ZERO( &write_fd_set ); // Zero out the set

  FD_SET( client_sck, &read_fd_set );  // Add the socket to the read set
  FD_SET( client_sck, &write_fd_set ); // Add the socket to the write set

  fd_set select_fd_storage[2];        // Make it easier to pass the sets around
  select_fd_storage[SELECT_READ_SETS]  = read_fd_set;
  select_fd_storage[SELECT_WRITE_SETS] = write_fd_set;
  // End select() FD set initialization


  // Bind Socket
   int status = -1;
   if( (status = bind( client_sck, ( struct sockaddr * )&client_rec, sizeof( client_rec ) )) < 0 )
     {
       printf( "Failed to bind client receiving socket!\n" );
       exit( EXIT_FAILURE );
     }
   printf( "Status = %d\n", status );
  // End Bind Socket


  // See if we have a hostname and not an IP address
  if( ( he = gethostbyname( server_address ) ) == NULL )
    {
      exit( EXIT_FAILURE ); // Bad Hostname
    }


  // Socket Settings
  memcpy( &server.sin_addr, he->h_addr_list[0], he->h_length );
  server.sin_family = AF_INET;
  server.sin_port = htons( port );


  // Read in the Binary File
  off_t file_size;
  struct stat stbuffer;
  int input_file = open( file_name, O_RDONLY ); // Crack open the binary file
  if( ( fstat( input_file, &stbuffer ) != 0 ) || ( !S_ISREG( stbuffer.st_mode ) ) )
    {
      printf( "Please provide a binary file!\n" );
      exit( EXIT_FAILURE ); // The file is probably not a binary file!
    }

  uint8_t* chunk = (uint8_t *)malloc( 1024 * sizeof(uint8_t) );
  int reader = 0;
  int message = 0;

  // Test that we can set these flags
  fake_tcp_header receiving_header;
  fake_tcp_header header;

  // Begin the three-way handshake
  perform_connection_setup( &header, client_sck, server, client_rec, address_size,
                            receiving_header, select_timeout, select_fd_storage );

  //print_header_values( header );

  perform_transfer( &header, client_sck, server, address_size, receiving_header,
                    select_timeout, select_fd_storage, input_file );

  close( client_sck );
  /* void * message_payload = malloc( 1040 * sizeof( uint8_t ) ); */
  /* int count = 0; */
  /* do */
  /*   { */
  /*     reader = read( input_file, chunk, 1024 ); */
  /*     header.sequence_number = count; */
  /*     header.acknowledgement_number = count; */
  /*     header.acknowledge = count; */
  /*     header.reset = count; */
  /*     header.window = count; */
  /*     header.checksum = 0; */
  /*     count++; */

  /*     // Checksum Calculation */
  /*     header.checksum = calculate_checksum( header, chunk, 1024 ); */
  /*     printf( "checksum = %d\n", header.checksum ); */
  /*     print_header_values( header ); */
  /*     memcpy( message_payload, &header, sizeof( header ) ); */


  /*     //print_bytes( chunk, 1024 ); */

  /*     message_payload += sizeof( header ); */
  /*     memcpy( message_payload, (uint8_t*)chunk, 1024 ); */

  /*     SHA1_Update( &ctx, message_payload, 1024 ); */
  /*     message_payload -= sizeof( header ); */

  /*     message = sendto( client_sck, message_payload, 1040, 0, (struct sockaddr *)&server, sizeof(server) ); */
  /*     //printf( "%d\n", message ); */
  /*     //printf( "Message Sent\n" ); */
  /*     //printf( "Reader = %d\n", reader ); */
  /*   } */
  /* while( reader > 0 ); */

  /* void * message_start = message_payload; */
  /* header.finish = 1; // Test telling the server that we're done. */
  /* //printf( "header.finish = %d\n", header.finish ); */

  /* memcpy( message_payload, &header, 16 ); */
  /* message = sendto( client_sck, message_start, 1040, 0, (struct sockaddr *)&server, sizeof(server) ); */
  /* //printf( "Sending FIN message = %d\n", message ); */
  /* //printf( "\n"); */

  /* //printf( "\n"); */
  /* // Memory Clean up */
  /* free( message_payload ); */
  /* //printf( "Size of uint8_t* = %d\n", sizeof(uint8_t*) ); */
  /* free( chunk ); */
  //free(buffer); // We'll need this for later
}
