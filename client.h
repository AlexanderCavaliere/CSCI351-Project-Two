/**
 * client.h
 * Alexander R. Cavaliere <arc6393@rit.edu>
 */

void client( char* file_name, int timeout, int verbose, char* server_address, int port );
