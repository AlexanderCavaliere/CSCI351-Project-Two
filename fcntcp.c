/**
 * fcntcp.c
 * Alexander R. Cavaliere <arc6393@rit.edu>
 */


#include <stdio.h>        // Print statements
#include <unistd.h>
#include <getopt.h>       // Command Line Args

#include <stdlib.h>       // Catch All
#include <sys/socket.h>   // UDP Sockets
#include <arpa/inet.h>    // Network interactions?

#include "client.h"
#include "server.h"

static struct option long_cla[] =
{
        {"client",  no_argument,       0, 'c'},
        {"server",  no_argument,       0, 's'},
        {"file",    required_argument, 0, 'f'},
        {"timeout", required_argument, 0, 't'},
        {"verbose", no_argument,       0, 'v'},
        {0 ,0, 0, 0, 0}
};

void main( int argc, char** argv )
{
        // Command Line Arguments
        int client_flag = 0; // Only one of these can be set
        int server_flag = 0; // Only one of these can be set
        char* file_name = NULL; // Client only
        int timeout = 1000; // 1000 ms by default
        int verbose = 0; // Client or Server
        char* server_address = NULL; // Either host name or IP address
        int port = -1; // Required by both client or server

        // Read in the command line arguments
        int opt = 0;
        int long_index = 0;
        while( (opt = getopt_long( argc, argv, "csf:t:v", long_cla, &long_index )) != -1 )
        {
                switch( opt )
                {
                case 'c':
                        if( server_flag == 1 )
                        {
                                printf(" You can not specify this as both a client and a server! Choose one!\n");
                                exit( EXIT_FAILURE );
                        }
                        client_flag = 1;
                        break;
                case 's':
                        if( client_flag == 1 )
                        {
                                printf(" You can not specify this as both a client and a server! Choose one!\n");
                                exit( EXIT_FAILURE );
                        }
                        server_flag = 1;
                        break;
                case 'f':
                        file_name = optarg;
                        break;
                case 't':
                        timeout = (int)strtol(optarg, (char**)NULL, 10);
                        break;
                case 'v':
                        verbose = 1;
                        break;
                }
        }
        // The server's address and port will exist at the end of the options
        // If we are the server we can ignore the server_address
        server_address = argv[argc-2];
        port = (int)strtol(argv[argc-1], (char**)NULL, 10);

        // Test if this works!
        printf( "client = %d\n", client_flag );
        printf( "server = %d\n", server_flag );
        printf( "file_name = %s\n", file_name );
        printf( "timeout = %d\n", timeout );
        printf( "verbose = %d\n", verbose );
        printf( "server_address = %s\n", server_address );
        printf( "port = %d\n", port );

        if( client_flag == 1 )
        {
                client( file_name, timeout, verbose, server_address, port );
        }
        if( server_flag == 1 )
        {
                server( verbose, port);
        }
}
