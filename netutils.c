#include "project_headers.h"
/* #include <stdio.h> */
/* #include <stdlib.h> */
/* #include <stdint.h> */
/* #include <unistd.h> */
/* #include <sys/types.h> */
/* #include <sys/stat.h> */
/* #include <sys/ioctl.h> */
/* #include <fcntl.h> */
/* #include <string.h> */

/* #include "netutils.h" */

void print_bytes( void * data, int data_size )
{
  for( int position = 0; position < data_size; ++position )
    {
      printf( "%02x ", ((uint8_t*)data)[position] );
      if( position % 9 == 0  && position != 0 )
         {
          printf( "\n" );
         }
    }
  printf( "\n" );
}

void print_header_values( fake_tcp_header header )
{
  printf( "Sequence number        = %u\n", header.sequence_number );
  printf( "Acknowledgement number = %u\n", header.acknowledgement_number );
  printf( "ACK                    = %u\n", header.acknowledge );
  printf( "RES                    = %u\n", header.reset );
  printf( "SYN                    = %u\n", header.sync );
  printf( "FIN                    = %u\n", header.finish );
  printf( "WND                    = %u\n", header.window );
  printf( "CHK                    = %u\n", header.checksum );
}


uint16_t calculate_checksum( fake_tcp_header head, uint8_t * data, int data_size )
{
  uint8_t * temp_buffer = (uint8_t *)malloc( (data_size + sizeof( head )) * sizeof( uint8_t) );
  uint8_t * temp_buffer_start = temp_buffer;

  // Write the header and data to a temporary buffer
  memcpy( temp_buffer, &head, sizeof( head ) );
  temp_buffer += sizeof( head );
  memcpy( temp_buffer, data, data_size );

  // Iterate over the buffer and perform the checksum calculation
  uint16_t word16;
  uint32_t checksum = 0;
  uint16_t position;

	// Make 16 bit words out of every two adjacent 8 bit words in the packet
	// and add them up
	for ( position = 0; position < ( data_size + sizeof( head ) ); position += 2 )
    {
		word16 = ( ( temp_buffer_start[position] << 8 ) & 0xFF00 )+( temp_buffer_start[position+1] & 0xFF );
		checksum += (uint32_t)word16;
	}

	// Take only 16 bits out of the 32 bit sum and add up the carries
	while ( checksum >> 16 )
	  checksum = ( checksum & 0xFFFF ) + ( checksum >> 16 );

	// One's complement the result
	checksum = ~checksum;

  // Clean up the used buffer
  temp_buffer -= sizeof( head );
  free( temp_buffer );

  // Return the checksum
  return ( (uint16_t)checksum );
}

bool verify_checksum( uint16_t prev_checksum, fake_tcp_header header, uint8_t * data, int data_size )
{
  header.checksum = 0;
  uint16_t recalculated_checksum = calculate_checksum( header, data, data_size );
  if( recalculated_checksum == prev_checksum )
    {
      return true;
    }
  return false;
}

// Change the values in the passed in header
void modify_header_values( fake_tcp_header * header, uint32_t sequence_number, uint32_t acknowledgement_number, uint8_t acknowledge,
                           uint8_t reset, uint8_t sync, uint8_t finish, uint16_t window, uint16_t checksum )
{
  header->sequence_number        = sequence_number;
  header->acknowledgement_number = acknowledgement_number;
  header->acknowledge            = acknowledge;
  header->reset                  = reset;
  header->sync                   = sync;
  header->finish                 = finish;
  header->window                 = window;
  header->checksum               = checksum;
}

void zero_out_header_values( fake_tcp_header * header )
{
  modify_header_values( header, 0, 0, 0, 0, 0, 0, 0, 0 );
}


void set_header_sequence_number( fake_tcp_header * header, uint32_t sequence_number )
{
  modify_header_values( header, sequence_number,
                        // Keep everything else the same
                        header->acknowledgement_number, header->acknowledge, header->reset,
                        header->sync, header->finish, header->window, header->checksum );
}


void set_header_acknowledgement_number( fake_tcp_header * header, uint32_t acknowledgement_number )
{
  modify_header_values( header, header->sequence_number,
                        acknowledgement_number, // Change the ack number
                        header->acknowledge, header->reset,
                        header->sync, header->finish, header->window, header->checksum );
}


void set_header_acknowledge_field( fake_tcp_header * header, uint8_t acknowledge )
{
  modify_header_values( header, header->sequence_number, header->acknowledgement_number,
                        acknowledge,
                        header->reset, header->sync, header->finish, header->window, header->checksum );
}

void set_header_reset_field( fake_tcp_header * header, uint8_t reset )
{
  modify_header_values( header, header->sequence_number, header->acknowledgement_number, header->acknowledge,
                        reset,
                        header->sync, header->finish, header->window, header->checksum );
}

void set_header_sync_field( fake_tcp_header * header, uint8_t sync )
{
  modify_header_values( header, header->sequence_number, header->acknowledgement_number, header->acknowledge, header->reset,
                        sync,
                        header->finish, header->window, header->checksum );
}

void set_header_finish_field( fake_tcp_header * header, uint8_t finish )
{
  modify_header_values( header, header->sequence_number, header->acknowledgement_number, header->acknowledge,
                        header->reset, header->sync,
                        finish,
                        header->window, header->checksum );
}

void set_header_window_field( fake_tcp_header * header, uint16_t window )
{
  modify_header_values( header, header->sequence_number, header->acknowledgement_number, header->acknowledge,
                        header->reset, header->sync, header->finish,
                        window,
                        header->checksum );
}

void set_header_checksum_field( fake_tcp_header * header, uint16_t checksum )
{
  modify_header_values( header, header->sequence_number, header->acknowledgement_number, header->acknowledge,
                        header->reset, header->sync, header->finish, header->window,
                        checksum );
}

void convert_header_to_host( fake_tcp_header * header )
{
  modify_header_values( header, ntohl(header->sequence_number), ntohl(header->acknowledgement_number), ntohs(header->acknowledge),
                        ntohs(header->reset), ntohs(header->sync), ntohs(header->finish), ntohs(header->window), ntohs(header->checksum) );
}


void convert_header_to_network( fake_tcp_header * header )
{
  modify_header_values( header, htonl(header->sequence_number), htonl(header->acknowledgement_number), htons(header->acknowledge),
                        htons(header->reset), htons(header->sync), htons(header->finish), htons(header->window), htons(header->checksum) );
}
