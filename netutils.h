/**
 *netutils.h
 *Alexander R. Cavaliere <arc6393@rit.edu>
 */

#define MIN_MSS 536
#define MAX_MSS 1440

struct in_flight_segment
{
  uint32_t sequence_number;
  uint8_t * data;
  uint8_t ACK;
  struct in_flight_segment * next_segment;
};

typedef struct in_flight_segment in_flight_segment;

struct fake_tcp_header
{
    uint32_t sequence_number;
    uint32_t acknowledgement_number;
    uint8_t  acknowledge;
    uint8_t  reset;
    uint8_t  sync;
    uint8_t  finish;
    uint16_t window;
    uint16_t checksum;
  }__attribute__((__packed__)); // 16 bytes

typedef struct fake_tcp_header fake_tcp_header;

struct payload
{
  fake_tcp_header header;
  uint8_t* data; // Anywhere from 536 to 1440
}__attribute__((__packed__)); // 16 bytes

typedef struct payload payload;

int calculate_timeout( int previous_timeout );

int estimate_RTT( int previous_RTT );

uint16_t calculate_maximum_segment_size( uint16_t previous_mss );

uint32_t calculate_next_sequence_number( uint32_t sequence_number_base, uint32_t payload_size );

uint16_t calculate_checksum( fake_tcp_header head, uint8_t * data, int data_size );

bool verify_checksum( uint16_t prev_checksum, fake_tcp_header header, uint8_t * data, int data_size );

void print_bytes( void* data, int data_size );

void print_header_values( fake_tcp_header header );

//void modify_header_values( fake_tcp_header * header, uint32_t sequence_number, uint32_t acknowledgement_number, uint8_t acknowledge,
//                           uint8_t reset, uint8_t sync, uint8_t finish, uint16_t window, uint16_t checksum );

void zero_out_header_values( fake_tcp_header * header );

void set_header_sequence_number( fake_tcp_header * header, uint32_t sequence_number );

void set_header_acknowledgement_number( fake_tcp_header * header, uint32_t acknowledgement_number );

void set_header_acknowledge_field( fake_tcp_header * header, uint8_t acknowledge );

void set_header_reset_field( fake_tcp_header * header, uint8_t reset );

void set_header_sync_field( fake_tcp_header * header, uint8_t sync );

void set_header_finish_field( fake_tcp_header * header, uint8_t finish );

void set_header_window_field( fake_tcp_header * header, uint16_t window );

void set_header_checksum_field( fake_tcp_header * header, uint16_t checksum );

void convert_header_to_host( fake_tcp_header * header );

void convert_header_to_network( fake_tcp_header * header );
