// Networking Libs
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

// Catch Alls
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

// Error Checking
#include <errno.h>

// Hashing
#include <openssl/sha.h>

// User Defined Headers
#include "netutils.h"

// STATICS
#define SELECT_READ_SETS  0
#define SELECT_WRITE_SETS 1
