/**
 * server.c
 * Alexander R. Cavaliere <arc6393@rit.edu>
 */

#include "project_headers.h"

void server( int verbose, int port )
{
  // Hash Variables
  SHA_CTX ctx;
  SHA1_Init( &ctx );

  // Socket Vars
  struct hostent* he;
  struct sockaddr_in server;
  struct sockaddr_in incoming;
  socklen_t address_size;

  // Socket Itself
  int server_sck = socket( AF_INET, SOCK_DGRAM, 0 );
  server.sin_family = AF_INET;
  server.sin_port = htons( port );
  server.sin_addr.s_addr = htonl( INADDR_ANY );

  // Make sure we've bound the server's socket
  int status = -1;
  if( (status = bind( server_sck, (struct sockaddr *)&server, sizeof(server) )) < 0 )
    {
      perror( "bind" );
      exit( EXIT_FAILURE );
    }
  printf( "Status = %d\n", status );

  // Buffer information
  int buffer_size = 1040;
  uint8_t* buffer = (uint8_t *)malloc( buffer_size * sizeof(uint8_t) );


  // User defined structs
  fake_tcp_header message_outgoing_header;
  fake_tcp_header message_received_header;


  zero_out_header_values( &message_outgoing_header );
  zero_out_header_values( &message_received_header );

  // Begin listening
  uint32_t last_ack;

  int message_out;
  uint32_t message_received_count = 0;
  uint32_t message_outgoing_count = 0;
  while( 1 )
    {
      printf( "%02x\n", *buffer );
      zero_out_header_values( &message_received_header );
      int bytes_recvd = recvfrom( server_sck, buffer, buffer_size,
                                  0, (struct sockaddr *)&incoming, &address_size );

      if( bytes_recvd < 0 )
        perror( "recvfrom" );

      printf( "bytes received = %d\n", bytes_recvd );
      printf("incoming address = %u\n", ntohl(incoming.sin_addr.s_addr) );
      printf("incoming port = %u\n",    ntohs(incoming.sin_port) );

      // Grab the header and adjust the buffer
      memmove( &message_received_header, buffer, sizeof( message_received_header) );

      //convert_header_to_host( &message_received_header );
      printf( "HEADER RECEIVED VALUES:\n" );
      print_header_values( message_received_header );


      if( bytes_recvd != sizeof( message_received_header) )
        buffer += sizeof( message_received_header );

      // Calculate the checksum of the received message
      /* uint16_t received_checksum = message_received_header.checksum; */
      /* message_received_header.checksum = 0; */
      /* uint16_t recalculated_checksum = calculate_checksum( message_received_header, buffer, */
      /*                                                      buffer_size - sizeof( message_received_header ) ); */
      /* message_received_header.checksum = received_checksum; */

      if( verify_checksum(  message_received_header.checksum, message_received_header, buffer, buffer_size - sizeof( message_received_header) ) );
      {
        printf( "RECEIVED MESSAGE CHECKSUM VERIFIED\n" );

        if( message_received_header.sync == 1)
          {
            set_header_sequence_number( &message_outgoing_header, 100 );
            set_header_acknowledgement_number( &message_outgoing_header, message_received_header.sequence_number + 1 );
            set_header_acknowledge_field( &message_outgoing_header, 1 );
            set_header_sync_field( &message_outgoing_header, 1 );
            set_header_checksum_field( &message_outgoing_header, 0 );

            set_header_checksum_field( &message_outgoing_header,
                                       calculate_checksum( message_outgoing_header,
                                                           (uint8_t *)&message_outgoing_header, 0 )
                                       );

            printf("OUTGOING HEADER VALUES:\n" );
            print_header_values( message_outgoing_header );

            message_out = sendto( server_sck, &message_outgoing_header, sizeof( message_outgoing_header ), 0,
                                  (struct sockaddr *)&incoming, sizeof(incoming) );

            last_ack = message_outgoing_header.acknowledgement_number;

            message_outgoing_count++;

            printf( "Message Out to Client = %d\n", message_out );
            if( message_out < 0 )
              perror("sendto");

            zero_out_header_values( &message_outgoing_header );


            if( bytes_recvd != sizeof( message_received_header) )
              buffer -= sizeof( message_received_header );

            printf( "COUNT OF SENT MESSAGES: %d\n", message_outgoing_count );
            continue;
          }
        if ( message_received_header.acknowledge == 1 )
          {

            printf( "Received Message ACK from Client.\n" );
            message_outgoing_count = 0;


            if( bytes_recvd != sizeof( message_received_header) )
              buffer -= sizeof( message_received_header );

            continue;
            // Do something here
          }

        if( message_received_header.finish == 1 )
          {
            printf( "Received Message Header FIN = %d\n", message_received_header.finish );
            break;
          } // Test Ending a message

        printf( "Data Segment Received.\n" );
        //print_bytes( buffer, buffer_size - sizeof( message_received_header ) );

        if( message_received_header.sequence_number < last_ack )
          {
            message_out = sendto( server_sck, &message_outgoing_header, sizeof( message_outgoing_header ), 0,
                                  (struct sockaddr *)&incoming, sizeof( incoming ) );

            message_outgoing_count++;

            printf( "COUNT OF SENT MESSAGES: %d\n", message_outgoing_count );
            continue;
          }

        set_header_sequence_number( &message_outgoing_header, message_received_header.acknowledgement_number + 1 );
        set_header_acknowledgement_number( &message_outgoing_header, message_received_header.sequence_number + bytes_recvd - sizeof( message_received_header ) );
        set_header_acknowledge_field( &message_outgoing_header, 1 );

        set_header_checksum_field( &message_outgoing_header, 0 );
        set_header_checksum_field( &message_outgoing_header,
                                   calculate_checksum( message_outgoing_header,
                                                       (uint8_t *)&message_outgoing_header, 0 ) );

        message_out = sendto( server_sck, &message_outgoing_header, sizeof( message_outgoing_header), 0,
                              (struct sockaddr *)&incoming, sizeof(incoming) );
        message_outgoing_count++;


        printf( "COUNT OF SENT MESSAGES: %d\n", message_outgoing_count );
        printf( "OUTGOING HEADER VALUES:\n" );
        print_header_values( message_outgoing_header );
        printf( "ACK Sent to Client\n" );
        if( message_out < 0 )
          perror( "sendto" );


      //print_header_values( message_received_header );
      //printf( "%d\n", bytes_recvd );
      //SHA1_Update( &ctx, (uint8_t*)buffer, buffer_size - sizeof( message_received_header ) );
      //print_bytes( buffer, buffer_size - sizeof( message_received_header ) );

      if( bytes_recvd != sizeof( message_received_header) )
        buffer -= sizeof( message_received_header );

      // Zero out the buffer at the end
      for( int position = 0; position < buffer_size; ++position )
        {
          buffer[position] = 0;
        }
      }// End Checksum Verification IF

      //printf( "Header Finish = %d\n", message_received_header.finish );
      //message_received_header.finish = 0;
      //printf( "Header Finish = %d\n", message_received_header.finish );
    }// for now we'll loop forever
  //printf( "\n" );

  unsigned char hash[SHA_DIGEST_LENGTH];
  SHA1_Final( hash, &ctx );
  print_bytes( hash, SHA_DIGEST_LENGTH );

  close( server_sck );
  //printf( "\n");
  //free( buffer );
}// end server
